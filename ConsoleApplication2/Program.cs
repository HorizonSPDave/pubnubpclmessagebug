﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PubnubApi;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            BrokenLibCall();

            WorkingLibCall();
        }

        static void BrokenLibCall()
        {
            Console.WriteLine("You should see a diagnostics output line with a publish timetoken");
            Console.WriteLine("The published message is also subscribed to here, and the status callback is called");
            Console.WriteLine("But the Message callback never fires, even though it should fire from the publish");
            Console.WriteLine("Here we go.....");
            var pn = new PortableLib.mypubnub(new Logger());
            Console.WriteLine("Start subscription");
            pn.StartSubscribe();

            Console.WriteLine("Wait 3 seconds");
            Thread.Sleep(3000);

            Console.WriteLine("Publish a message");
            pn.DoPublish();

            Console.WriteLine("Waiting for an ENTER keypress.....");
            Console.WriteLine("(But presumably, no message callback has fired.....)");
            Console.ReadLine();

        }

        static void WorkingLibCall()
        {
            Console.WriteLine("----------------------------------------------------------------------------------");
            Console.WriteLine("Now the non-PCL version");
            Console.WriteLine("You should see a diagnostics output line with a publish timetoken");
            Console.WriteLine("The published message is also subscribed to here, and the status callback is called");
            Console.WriteLine("AND the Message callback fires correctly from the Publish!");
            Console.WriteLine("Here we go.....");
            var pn = new Pubnublib.mypubnub(new Logger());
            Console.WriteLine("Start subscription");
            pn.StartSubscribe();

            Console.WriteLine("Wait 3 seconds");
            Thread.Sleep(3000);

            Console.WriteLine("Publish a message");
            pn.DoPublish();

            Console.WriteLine("Waiting for an ENTER keypress.....");
            Console.ReadLine();

        }

        public class Logger : PortableLib.IConsoleLogger
        {
            public void Log(string msg)
            {
                Console.WriteLine(msg);
            }
        }
    }


}
