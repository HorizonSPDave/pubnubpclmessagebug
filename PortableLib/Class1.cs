﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PubnubApi;

namespace PortableLib
{
    public class mypubnub
    {
        private static IConsoleLogger logger;
        private Pubnub _pubnub { get; set; }

        public static void msgcallback(Pubnub p, PNMessageResult<object> m)
        {
            logger.Log("Message! BUT THIS NEVER HAPPENS!");
            if (m.Message != null)
            {
                logger.Log("Ch=" + m.Channel + "<br/>" + "TT=" + m.Timetoken);
                var myMsg = m.Message;
            }
        }

        public void StartSubscribe()
        {
            _pubnub.Subscribe<string>()
                .Channels(new string[] {
                        // subscribe to channels
                        "DEVspeciality2"
                 })
                .Execute();

            SubscribeCallbackExt listenerSubscribeCallack = new SubscribeCallbackExt(
                mypubnub.msgcallback,
                (pubnubObj, presence) =>
                {
                    logger.Log("Presence");
                    // handle incoming presence data 
                },
                (pubnubObj, status) =>
                {
                    // the status object returned is always related to subscribe but could contain
                    // information about subscribe, heartbeat, or errors
                    // use the PNOperationType to switch on different options
                    logger.Log("Status");
                });

            _pubnub.AddListener(listenerSubscribeCallack);
        }

        public void DoPublish()
        {
            _pubnub.Publish()
                            .Channel("DEVspeciality2")
                            .Message("{'hello':'world'}")
                            .Async(new DemoPublishResult());
        }

        public mypubnub(IConsoleLogger iLog)
        {
            logger = iLog;
            PNConfiguration pnConfiguration = new PNConfiguration
            {
                PublishKey = "demo",
                SubscribeKey = "demo",
                Secure = true
            };

            _pubnub = new Pubnub(pnConfiguration);

        }
    }
    public class DemoPublishResult : PNCallback<PNPublishResult>
    {
        public override void OnResponse(PNPublishResult result, PNStatus status)
        {
            System.Diagnostics.Debug.WriteLine($"Publish={result.Timetoken}");
        }
    };

    public interface IConsoleLogger
    {
        void Log(string msg);
    }
}
